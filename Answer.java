public class Answer {

    public Answer() {}

    /*
    * PROBLEM A
    * Pendekatan saya adalah menggunakan Selection Sort, karena 
    * sorting itulah yang sudah menjadi "auto pilot" saya.
    * 
    * Notes:
    * Sempat berpikir menggunakan Quick Sort, namun karena ragu dengan implementasi
    * partitionnya serta demi tetap menjaga integritas, akhirnya saya memutuskan
    * tetap menggunakan Selection Sort (Walaupun Quick Sort lebih baik)   
    */
    public int[] solveA(int arr[]) {
  
        for(int i=0; i<arr.length-1; i++) {
            int minIdx = i;

            for(int j=i+1; j<arr.length; j++){
                if(arr[j] < arr[minIdx])
                    minIdx = j;
            }

            int temp = arr[minIdx];
            arr[minIdx] = arr[i];
            arr[i] = temp;
        }

        return arr;
    }


    /*
    * PROBLEM B
    * Pendekatan saya untuk problem ini sederhana, yaitu:
    * - Buat variabel baru untuk menyimpan indeks terakhir array 
    * - looping array hingga indeks ke length/2
    * - Melakukan operasi dan menyimpannya sesuai ketentuan
    * - Kurangi lastIdx
    *
    * Kurang lebih konsepnya partitioning array hingga elemen tengah
    */
    public int[] solveB(int[] arr){

        int lastIdx = arr.length-1;

        for(int i=0; i<arr.length/2; i++){
            int temp = arr[i] * arr[lastIdx];
            arr[i] = temp;
            arr[lastIdx] = temp;

            lastIdx--;
        }

        return arr;
    }


    /*
    * PROBLEM C
    * Pendekatan saya untuk problem ini juga sederhana, yaitu:
    * - Base case jika arr kosong atau hanya ada satu elemen 
    * - looping array hingga indeks ke length-2
    * - Cek apakah indeks ganjil atau genap, lalu lakukan operasi
    * - Simpan hasil operasi ke array baru sesuai indeks
    */
    public int[] solveC(int[] arr){

        if(arr.length==0 || arr.length==1)
            return arr;

        int[] newArr = new int[arr.length-1];

        for(int i=0; i<arr.length-1; i++){
            if(i%2==0)
                newArr[i] = arr[i] + arr[i+1];
            else
                newArr[i] = arr[i] - arr[i+1];
        }

        return newArr;
    }

}
