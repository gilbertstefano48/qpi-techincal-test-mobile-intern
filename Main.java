import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Answer answer = new Answer();

        /* Please modify this array */
        int[] arrayA = {10, 33, 13, 72, 44, 51, 17};

        // Copy array untuk digunakan problem B dan C
        int[] arrayB = arrayA.clone();
        int[] arrayC = arrayA.clone();
        
        // Print jawaban untuk problem A
        System.out.println("Problem A");
        System.out.println(Arrays.toString(answer.solveA(arrayA)) + "\n");

        // Print jawaban untuk problem B
        System.out.println("Problem B");
        System.out.println(Arrays.toString(answer.solveB(arrayB)) + "\n");

        // Print jawaban untuk problem C
        System.out.println("Problem C");
        System.out.println(Arrays.toString(answer.solveC(arrayC)) + "\n");

    }
}
