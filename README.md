# QPI Techincal Test Mobile Intern

Jawaban untuk QPI Technical Test Mobile Intern soal 2 (Technical Test)

## Description

Repository ini terdiri atas 2 file utama: **Main.java** dan **Answer.java**

Ketiga fungsi yang merupakan jawaban dari soal terdapat pada file **Answer.java**

Sebagai catatan, saya telah memberikan sedikit deskripsi mengenai pendekatan saya terhadap soal yang diberikan di atas setiap fungsi terkait.

## Usage

Pada fungsi main di **Main.java**, telah tersedia sebuah array yang dapat dimodifikasi sesuai keperluan testing. 

```java
/* Please modify this array */
int[] arrayA = {10, 33, 13, 72, 44, 51, 17};
```

Layaknya program Java pada umumnya, untuk menjalankan program cukup run fungsi main. Berikut adalah contoh output dari program:

```
Problem A
[10, 13, 17, 33, 44, 51, 72]

Problem B
[170, 1683, 572, 72, 572, 1683, 170]

Problem C
[43, 20, 85, 28, 95, 34]
```

## Authors and acknowledgment
Gilbert Stefano W (26 Februari 2022 10.30 WIB)
